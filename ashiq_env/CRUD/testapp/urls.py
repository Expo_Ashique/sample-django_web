from django.conf.urls import include, url
from . import views

app_name = 'testapp'
urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),

    url(r'person/add/$', views.PersonCreate.as_view(), name='person-add'),
    url(r'person/(?P<pk>[0-9]+)/$', views.PersonUpdate.as_view(), name='person-update'),
    url(r'person/(?P<pk>[0-9]+)/delete/$', views.PersonDelete.as_view(), name='person-delete'),
]
