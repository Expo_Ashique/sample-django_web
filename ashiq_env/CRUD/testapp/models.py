from django.db import models
from django.views import generic
from django.core.urlresolvers import reverse
from django.views.generic.edit import CreateView, UpdateView, DeleteView


# Create your models here.
class Person(models.Model):
    name = models.CharField(max_length=250)
    country= models.CharField(max_length=500)


    def get_absolute_url(self):
        return reverse('testapp:detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.name + " - " + self.country


class Description(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    adress = models.CharField(max_length=220)
    mobile_number= models.CharField(max_length=250)
    favorite_food = models.CharField(max_length=250)

    def __str__(self):
        return self.adress+" - "+self.mobile_number+" - "+self.favorite_food


class PersonCreate(CreateView):
    model = Person
    fields = ['name', 'country' ]

