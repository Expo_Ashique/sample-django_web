from django.views import generic
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from .models import Person
from django.template import loader

class IndexView(generic.ListView):
    template_name='testapp/index.html'
    context_object_name='all_persons'
    def get_queryset(self):
        return Person.objects.all()

class DetailView(generic.DetailView):
    model=Person
    template_name = 'testapp/detail.html'

class PersonCreate(CreateView):
    model=Person
    fields=['name','country']
class PersonUpdate(UpdateView):
    model=Person
    fields=['name','country' ]
class PersonDelete(DeleteView ):
    model=Person
    success_url=reverse_lazy('testapp:index')